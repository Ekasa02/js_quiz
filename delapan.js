/* Fungsi untuk membalikkan arr[] dari awal sampai akhir*/
function reverseArray(arr,awal,akhir){
    while (awal<akhir){
        var one=arr[awal];
        arr[awal]=arr[akhir];
        arr[akhir]=one;
        awal++;
        akhir--;
    }
}
/* Fungsi utilitas untuk mencetak array */
function printArray(arr,A){
    console.log(arr)
    // for(var i=0;i<A;i++){
    //     console.log(arr[i]);
    // }
}
/* Fungsi driver untuk menguji fungsi di atas */
var arr=[8,2,9,4];
var n=4;
// Untuk mencetak array asli
printArray(arr,n);

// pemanggilan fungsi
reverseArray(arr,0,n-1);

// Untuk mencetak array Terbalik
printArray(arr,n);

